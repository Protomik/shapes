#ifndef POINT_H
#define POINT_H

class Point {
public:
	Point();
	Point(double x, double y);
	Point(const Point& other);
	double getX();
	void setX(double x);
	double getY();
	void setY(double y);
	void setCoordinates(double x, double y);
	void moveCoordinates(double dx, double dy);
	void moveLeft(double dx);
	void moveRight(double dx);
	void moveUp(double dy);
	void moveDown(double dy);

private:
	double x;
	double y;
	double dx;
	double dy;
};

#endif