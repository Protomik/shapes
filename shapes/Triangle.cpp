#include "Point.h"
#include "Triangle.h"
#include <math.h>

Triangle::Triangle(double ax, double ay, double bx, double by, double cx, double cy){
	this->ax = ax;
	this->ay = ay;
	this->bx = bx;
	this->by = by;
	this->cx = cx;
	this->cy = cy;
};

Triangle::Triangle(const Triangle& other){
	this->ax = other.ax;
	this->ay = other.ay;
	this->bx = other.bx;
	this->by = other.by;
	this->cx = other.cx;
	this->cy = other.cy;
};

Triangle::Triangle(Point pointA, Point pointB, Point pointC){  
	this->ax = pointA.getX();
	this->ay = pointA.getY();
	this->bx = pointB.getX();
	this->by = pointB.getY();
	this->cx = pointC.getX();
	this->cy = pointC.getY();
};

double Triangle::getAB(){
	return sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay));
};

double Triangle::getBC(){
	return sqrt((cx - bx)*(cx - bx) + (cy - by)*(cy - by));
};

double Triangle::getAC(){
	return sqrt((cx - ax)*(cx - ax) + (cy - ay)*(cy - ay));
};

double Triangle::getArea(){
	double pAB = sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay));
	double pBC = sqrt((cx - bx)*(cx - bx) + (cy - by)*(cy - by));
	double pAC = sqrt((cx - ax)*(cx - ax) + (cy - ay)*(cy - ay));
	double p = (pAB + pBC + pAC) / 2;
	return sqrt(p*(p - pAB)*(p - pBC)*(p - pAC));
};

double Triangle::getPerimeter(){
	double prAB = sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay));
	double prBC = sqrt((cx - bx)*(cx - bx) + (cy - by)*(cy - by));
	double prAC = sqrt((cx - ax)*(cx - ax) + (cy - ay)*(cy - ay));
	return prAB + prBC + prAC;
};