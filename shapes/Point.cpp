#include "Point.h"

Point::Point(){
	this->x = 0;
	this->y = 0;
};

Point::Point(double x, double y){
	this->x = x;
	this->y = y;
};

Point::Point(const Point& other){
	this->x = other.x;
	this->y = other.y;
};

double Point::getX(){
	return this->x;
};

void Point::setX(double x){
	this->x = x;
};

double Point::getY(){
	return this->y;
};

void Point::setY(double y){
	this->y = y;
};

void Point::setCoordinates(double x, double y){
	this->x = x;
	this->y = y;
};

void Point::moveCoordinates(double dx, double dy){
	this->x += dx;
	this->y += dy;
};

void Point::moveLeft(double dx){
	this->x -= dx;
};

void Point::moveRight(double dx){
	this->x += dx;
};

void Point::moveUp(double dy){
	this->y -= dy;
};

void Point::moveDown(double dy){
	this->y += dy;
};