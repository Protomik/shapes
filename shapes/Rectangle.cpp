#include "Point.h"
#include "Rectangle.h"
#include <math.h>



Rectangle::Rectangle(double ax, double ay, double bx, double by, double cx, double cy, double dx, double dy){
	this->ax = ax;
	this->ay = ay;
	this->bx = bx;
	this->by = by;
	this->cx = cx;
	this->cy = cy;
	this->dx = dx;
	this->dy = dy;
};

Rectangle::Rectangle(const Rectangle& other){
	this->ax = other.ax;
	this->ay = other.ay;
	this->bx = other.bx;
	this->by = other.by;
	this->cx = other.cx;
	this->cy = other.cy;
	this->dx = other.dx;
	this->dy = other.dy;
};

Rectangle::Rectangle(Point pointA, Point pointB, Point pointC, Point pointD){
	this->ax = pointA.getX();
	this->ay = pointA.getY();
	this->bx = pointB.getX();
	this->by = pointB.getY();
	this->cx = pointC.getX();
	this->cy = pointC.getY();
	this->dx = pointD.getX();
	this->dy = pointD.getY();
};

double Rectangle::getFirstSide(){
	return sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay));
};

double Rectangle::getSecondSide(){
	return sqrt((dx - ax)*(dx - ax) + (dy - ay)*(dy - ay));
};

double Rectangle::getArea(){
	double rAB = sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay));
	double rAD = sqrt((dx - ax)*(dx - ax) + (dy - ay)*(dy - ay));
	return rAB * rAD;
};

double Rectangle::getPerimeter(){
	double rAB = sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay));
	double rAD = sqrt((dx - ax)*(dx - ax) + (dy - ay)*(dy - ay));
	return rAB * 2 + rAD * 2;
};