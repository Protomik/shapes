#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Point.h"

class Triangle {
public:
	Triangle(double ax, double ay, double bx, double by, double cx, double cy);

	Triangle(const Triangle& other);

	Triangle(Point pointA, Point pointB, Point pointC);

	double getAB();

	double getBC();

	double getAC();

	double getArea();

	double getPerimeter();

private:
	double ax;
	double ay;
	double bx;
	double by;
	double cx;
	double cy;
	Point pointA;
	Point pointB;
	Point pointC;
};

#endif