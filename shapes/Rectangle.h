#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Point.h"

class Rectangle{
public:
	Rectangle(double ax, double ay, double bx, double by, double cx, double cy, double dx, double dy);

	Rectangle(Point pointA, Point pointB, Point pointC, Point pointD);

	Rectangle(const Rectangle& other);

	double getFirstSide();

	double getSecondSide();

	double getArea();

	double getPerimeter();

private:
	double ax;
	double ay;
	double bx;
	double by;
	double cx;
	double cy;
	double dx;
	double dy;
	Point pointA;
	Point pointB;
	Point pointC;
	Point pointD;

};

#endif