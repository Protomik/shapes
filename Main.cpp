#include "shapes/Point.h"
#include "shapes/Triangle.h"
#include "shapes/Rectangle.h"
#include <iostream>

int main(int argc, char ** argv) {

	
	Point point(50, 100);

	std::cout << "Point(50, 100)"<< std::endl;
	std::cout << "x = " << point.getX() << " y = " << point.getY() << std::endl;

	point.moveUp(20);
	point.moveLeft(20);

	std::cout << "x = " << point.getX() << " y = " << point.getY() << std::endl;

	point.moveCoordinates(55,-55);

	std::cout << "x = " << point.getX() << " y = " << point.getY() << std::endl;
	std::cout << std::endl;
	
	Point * pointZero = new Point();

	std::cout << "Point()" << std::endl;
	std::cout << "x = " << pointZero->getX() << " y = " << pointZero->getY() << std::endl;
	std::cout << std::endl;

	Point * pointPoint = new Point(point);

	std::cout << "Point(point(50, 100))"<< std::endl;
	std::cout << "x = " << pointPoint->getX() << " y = " << pointPoint->getY() << std::endl;
	std::cout << std::endl;	

	Triangle triangle(1, 10, 4, 14, 0, 0);
	
	std::cout << "Triangle(1, 10, 4, 14, 0, 0)" << std::endl;
	std::cout << "        AB = " << triangle.getAB() << std::endl;
	std::cout << "        BC = " << triangle.getBC() << std::endl;
	std::cout << "        AC = " << triangle.getAC() << std::endl;
	std::cout << " Perimeter = " << triangle.getPerimeter() << std::endl;
	std::cout << "      Area = " << triangle.getArea() << std::endl;
	std::cout << std::endl;

	Point pointA(6, 15);
	Point pointB(9, 29);
	Point pointC(5, 5);

	Triangle trianglePoint(pointA, pointB, pointC);

	std::cout << "Triangle(pointA(6, 15), pointB(9, 29), pointC(5, 5))" << std::endl;
	std::cout << "        AB = " << trianglePoint.getAB() << std::endl;
	std::cout << "        BC = " << trianglePoint.getBC() << std::endl;
	std::cout << "        AC = " << trianglePoint.getAC() << std::endl;
	std::cout << " Perimeter = " << trianglePoint.getPerimeter() << std::endl;
	std::cout << "      Area = " << trianglePoint.getArea() << std::endl;
	std::cout << std::endl;

	Triangle triangleTriangle(triangle);

	std::cout << "Triangle(triangle(1, 10, 4, 14, 0, 0))" << std::endl;
	std::cout << "        AB = " << triangleTriangle.getAB() << std::endl;
	std::cout << "        BC = " << triangleTriangle.getBC() << std::endl;
	std::cout << "        AC = " << triangleTriangle.getAC() << std::endl;
	std::cout << " Perimeter = " << triangleTriangle.getPerimeter() << std::endl;
	std::cout << "      Area = " << triangleTriangle.getArea() << std::endl;
	std::cout << std::endl;

	Rectangle rectangle(2, 2, 9, 2, 9, 5, 2, 5);
	std::cout << "Rectangle(2, 2, 9, 2, 9, 5, 2, 5)" << std::endl;
	std::cout << "        AB = " << rectangle.getFirstSide() << std::endl;
	std::cout << "        AD = " << rectangle.getSecondSide() << std::endl;
	std::cout << " Perimeter = " << rectangle.getPerimeter() << std::endl;
	std::cout << "      Area = " << rectangle.getArea() << std::endl;
	std::cout << std::endl;

	pointA.setX(16);
	pointA.setY(4);
	pointB.setX(25);
	pointB.setY(4);
	pointC.setX(25);
	pointC.setY(10);
	Point pointD(16, 10);

	Rectangle rectanglePoint(pointA, pointB, pointC, pointD);
	std::cout << "Rectangle(pointA(16, 4), pointB(25, 4), pointC(25, 10), pointD(16, 10))" << std::endl;
	std::cout << "        AB = " << rectanglePoint.getFirstSide() << std::endl;
	std::cout << "        AD = " << rectanglePoint.getSecondSide() << std::endl;
	std::cout << " Perimeter = " << rectanglePoint.getPerimeter() << std::endl;
	std::cout << "      Area = " << rectanglePoint.getArea() << std::endl;
	std::cout << std::endl;

	Rectangle rectangleRectangle(rectangle);
	std::cout << "Rectangle(rectangle(2, 2, 9, 2, 9, 5, 2, 5))" << std::endl;
	std::cout << "        AB = " << rectangleRectangle.getFirstSide() << std::endl;
	std::cout << "        AD = " << rectangleRectangle.getSecondSide() << std::endl;
	std::cout << " Perimeter = " << rectangleRectangle.getPerimeter() << std::endl;
	std::cout << "      Area = " << rectangleRectangle.getArea() << std::endl;
	std::cout << std::endl;

	return 0;
}